# Browser Rendering

## Browser Components
![Browser Components](https://miro.medium.com/max/700/1*lMBu87MtEsVFqqbfMum-kA.png) 


### User Interface: 

Everything which we see but we cannot manipulate like address bar, settings of the browser, forward and backward buttons, bookmarking menu, etc. It works together with UI backend which is designed to develop basic widgets. 

### Browser Engine: 

It works as an intermediate between user interface and rendering engine. If you do anything in the browser, it takes the command and execute it. 

### Rendering Engine: 

It parses the HTML, CSS, JavaScript and based on the results which is being parsed is going to display the page. Similar to the JavaScript engines, different browsers use different rendering engines as well. These are some of the popular ones: 

* **Gecko** — Firefox
* **WebKit** — Safari
* **Blink** — Chrome, Opera (from version 15 onwards)

### Networking: 

When you have an HTTPS or an HTTP request, networking layer is going to make sure that the resources are loaded. 

### JavaScript Engine: 

It is going to interpret the JS ans the data persistence like cookies, local storage, index DB, file system, etc. 


## Process of Browser Rendering 

```mermaid
graph LR; 
    Parse_HTML-->DOM; 
    Parse_CSS-->CSSOM; 
    DOM-->Render_Tree; 
    CSSOM-->Render_Tree; 
    Render_Tree-->Layout; 
    Layout-->Paint; 
``` 

To get a webpage, first we select a web browser which can also be called as client browser. We click on a link or type a URL(uniform resource locator) and the browser goes out to find the server that corresponds to the first part of the URL. That server takes the URL and finds the file or the other resource whatever a specified deep in its own file system.The URL specifies both the server and the actual files that the browser wants to fetch and the server returns it to the browser. 

Usually this is an HTML file, so the browser parses it. Parsing means it looks at each and every part of the HTML file, interprets it and then acts on them. HTML contains the text content of the webpage and it also contains the information about the structure and links to other files. The other files can be media like pictures, videos, etc, cascading style sheets contains the information about the appearence of the webpage and javascript files contains the information about the behavior of the webpage. 

Webpage has three parts. 
* Content 
* Appearence 
* Behavior 

The browser gets all the files and it combines them all into the DOM(document object model) and it renders the page(draws the page in the window of the web browser). DOM represents the contents of HTML document as tree structure. Using it, we can easily read, access, update the contents of the document. 


```mermaid
graph LR; 
    Docunet-->files; 
    Object-->tags_or_elements; 
    Model-->Layout_or_Structure; 
``` 

## Parsing to construct DOM Tree 

![Parsing](https://image.slidesharecdn.com/howmodernbrowserswork-180609081043/95/neoito-how-modern-browsers-work-23-638.jpg?cb=1535537638) 

It can be defined as translating a document into a structure that code can use. CSS and JS uses convetional parser as they are context free grammar. HTML uses non-convetional parser 
as it is not context free grammar. 


For example, if we have the HTML document: 

```
<html>
  <head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="theme.css">
  </head>
  <body>
    <p> Hello, <span> Students </span> </p>
    <div> 
      <img src="smiley.gif" alt="Smiley face" height="42" width="42">
    </div>
  </body>
</html>
```

The DOM tree for this HTML looks like: 

```mermaid
graph TD; 
    HTML-->HEAD; 
    HTML-->BODY; 
    HEAD-->META; 
    HEAD-->LINK; 
    BODY-->P; 
    BODY-->DIV; 
    DIV-->IMG; 
    P-->Hello; 
    P-->SPAN; 
    SPAN-->Students; 
``` 

Basically, each element is represented as the parent node to all of the elements, which are directly contained inside of it. And this is applied recursively. 

## Constructing the CSSOM tree 

CSSOM refers to the CSS Object Model. While the browser was constructing the DOM of the page, it encountered a link tag in the head section which was referencing the external theme.css CSS style sheet. Anticipating that it might need that resource to render the page, it immediately dispatched a request for it. Let’s imagine that the theme.css file has the following contents: 

``` 
body { 
  font-size: 16px;
}

p { 
  font-weight: bold; 
}

span { 
  color: red; 
}

p span { 
  display: none; 
}

img { 
  float: right; 
}
``` 

Here is how the CSSOM tree will look like: 

![CSSOM](https://miro.medium.com/max/700/1*5YU1su2mdzHEQ5iDisKUyw.png) 

## Constructing the render tree 

* Render tree will be generated while DOM tree is constructed. 
* Visual elements in the order which they are going to be displayed. 
* Elements in the render tree are called renderer or render objects. 
* Render object is a rectangle. 

This is how the renderer tree of the above DOM and CSSOM trees will look like: 

```mermaid
graph TD; 
    BODY-->P; 
    BODY-->DIV; 
    BODY-->font-size:16px; 
    P-->font-size:16px,font-weight:bold; 
    P-->Hello; 
    P-->Students; 
    DIV-->IMG; 
    IMG-->font-size:16px,float:right; 
```

## Layout of the render tree 

* When the renderer is created and added to the tree, it does not have a position and size. Calculating these values is called layout. 
* Most of the time possible to compute geometry in one pass. 
* Recursive process begins at the root object(<html>). 

## Painting the render tree 

The renderer tree is traversed and the renderer’s paint() method is called to display the content on the screen. 

Painting can be global or incremental (similar to layout): 

* **Global** — the entire tree gets repainted. 
* **Incremental** — only some of the renderers change in a way that does not affect the entire tree. The renderer invalidates its rectangle on the screen. This causes the OS to see it as a region that needs repainting and to generate a paint event. The OS does it in a smart way by merging several regions into one. 

## References 

* https://www.youtube.com/watch?v=0IsQqJ7pwhw
* https://www.youtube.com/watch?v=n1cKlKM3jYI
* https://www.youtube.com/watch?v=SmE4OwHztCc&t=1522s
* https://www.youtube.com/watch?v=DuSURHrZG6I
* https://www.youtube.com/watch?v=KM9coMpy5sQ&t=394s
* https://www.youtube.com/watch?v=XD7fYLQeQIg&t=285s
* https://www.youtube.com/watch?v=0ik6X4DJKCc&t=2054s
* https://www.youtube.com/watch?v=rlzF8TjfrWo
* https://www.youtube.com/watch?v=ipkjfvl40s0
* https://blog.sessionstack.com/how-javascript-works-the-rendering-engine-and-tips-to-optimize-its-performance-7b95553baed
* https://medium.com/jspoint/how-the-browser-renders-a-web-page-dom-cssom-and-rendering-df10531c9969